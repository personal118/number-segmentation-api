import os

import redis
from rq import Worker, Queue, Connection, SimpleWorker
from numbers_segmentation_api import config

from numbers_segmentation_api.queue.models.unet import unet
import numpy as np

listen = ['default']

config = config.get_config(os.getenv('FLASK_ENV', 'development'))

conn = redis.from_url(config.REDIS_URI)

if __name__ == '__main__':
    with Connection(conn):
        worker = SimpleWorker(list(map(Queue, listen)))
        worker.work()
