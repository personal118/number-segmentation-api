import uuid


def get_image_from_predict(y,path):
	import numpy as np
	from matplotlib import patches as mpatches
	import matplotlib.pyplot as plt

	print('color')
	colors = np.array([
		[1, 2, 1],
		[0, 2, 2],
		[1, 0, 3],
		[0, 1, 3],
		[1, 2, 3],
		[1, 3, 2],
		[2, 1, 3],
		[2, 3, 1],
		[3, 1, 2],
		[3, 2, 1],
	]) / 3

	def colorize_target(target):
		output_array = np.zeros((target.shape[0], target.shape[1], 3))
		for index in range(0,10):
			output_array[target[:,:,index] > 0.5] = colors[index]
		return output_array

	legend_handles = []
	for i in range(0,10):
		legend_handles.append(mpatches.Patch(color=colors[i], label=str(i)))

	plt.rcParams['figure.figsize'] = (8,8) # Make the figures a bit bigger
	plt.imshow(colorize_target(y))
	plt.legend(handles=legend_handles, bbox_to_anchor=(1, 1), loc='upper left')
	plt.savefig(path)

def segmentation(image_path):
	from numbers_segmentation_api.queue.models.unet import unet
	import numpy as np
	import PIL
	import os

	im = PIL.Image.open(image_path)
	old_width, old_height = im.size	
	im = im.resize(size=(256,256)).convert('L')
	im = PIL.ImageOps.invert(im)
	X = np.array(im).reshape((1,256,256,1)) / 255

	predicted_y = unet.predict(X)
	returned_path = f"/uploads/{uuid.uuid4()}.jpg"
	save_path = f"./static{returned_path}"
	get_image_from_predict(predicted_y[0], save_path)
	return returned_path

