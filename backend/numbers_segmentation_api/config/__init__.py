"""Config settings for for development, testing and production environments."""
import os
from pathlib import Path

HERE = Path(__file__).parent
# SQLITE_DEV = "sqlite:///" + str(HERE / "flask_api_tutorial_dev.db")
# SQLITE_TEST = "sqlite:///" + str(HERE / "flask_api_tutorial_test.db")
# SQLITE_PROD = "sqlite:///" + str(HERE / "flask_api_tutorial_prod.db")
POSTGRES_DEV = "postgresql://postgres:postgres@localhost:5432/"
POSTGRES_TEST = "postgresql://postgres:postgres@localhost:5432/"
POSTGRES_PROD = ""
REDIS_DEV = "redis://localhost:6379"
REDIS_TEST = "redis://localhost:6379"
REDIS_PROD = ""

path = os.getcwd()
# file Upload
UPLOAD_FOLDER = os.path.join(path, 'static')
# Make directory if "uploads" folder not exists
if not os.path.isdir(UPLOAD_FOLDER):
    os.mkdir(UPLOAD_FOLDER)

UPLOAD_FOLDER = os.path.join(path, 'static/uploads')
# Make directory if "uploads" folder not exists
if not os.path.isdir(UPLOAD_FOLDER):
    os.mkdir(UPLOAD_FOLDER)

class Config:
    """Base configuration."""

    # Секретный ключ для формирования JWT токена
    SECRET_KEY = os.getenv("SECRET_KEY", "open sesame")  
    # Кол-во раундов при хэшировании
    BCRYPT_LOG_ROUNDS = 4
    # Время жизни токена часы
    TOKEN_EXPIRE_HOURS = 0
    # Время жизни токена минуты
    TOKEN_EXPIRE_MINUTES = 0
    # ОРМ должен отслеживать модфикацию сущностей
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SWAGGER_UI_DOC_EXPANSION = "list"
    RESTX_MASK_SWAGGER = False
    JSON_SORT_KEYS = False
    #It will allow below 16MB contents only, you can change it
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    UPLOAD_FOLDER = UPLOAD_FOLDER


class TestingConfig(Config):
    """Testing configuration."""

    TESTING = True
    SQLALCHEMY_DATABASE_URI = POSTGRES_TEST
    REDIS_URI = REDIS_TEST


class DevelopmentConfig(Config):
    """Development configuration."""

    TOKEN_EXPIRE_MINUTES = 15
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", POSTGRES_DEV)
    REDIS_URI = os.getenv("REDIS_URL", REDIS_DEV)
class ProductionConfig(Config):
    """Production configuration."""

    TOKEN_EXPIRE_HOURS = 1
    BCRYPT_LOG_ROUNDS = 13
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", POSTGRES_PROD)
    PRESERVE_CONTEXT_ON_EXCEPTION = True
    REDIS_URI = os.getenv("REDIS_URL", REDIS_PROD)


ENV_CONFIG_DICT = dict(
    development=DevelopmentConfig, testing=TestingConfig, production=ProductionConfig
)


def get_config(config_name):
    """Retrieve environment configuration settings."""
    return ENV_CONFIG_DICT.get(config_name, ProductionConfig)
