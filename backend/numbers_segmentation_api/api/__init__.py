"""API blueprint configuration."""
from flask import Blueprint
from flask_restx import Api

from numbers_segmentation_api.api.segmentation.endpoints import segmentation_ns
# from numbers_segmentation_api.api.messages.endpoints import messages_ns

api_bp = Blueprint("api", __name__, url_prefix="/api/v1")
# authorizations = {"Bearer": {"type": "apiKey", "in": "header", "name": "Authorization"}}

api = Api(
    api_bp,
    version="1.0",
    title="Numbers Segmentation API",
    description="Hadles for some models which can segmentation images 256*256",
    doc="/docs",
    # authorizations=authorizations,
)

api.add_namespace(segmentation_ns, path="/segmentation")
# api.add_namespace(messages_ns, path="/messages")
