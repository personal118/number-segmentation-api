from flask_restx import Namespace

# from chat_api.api.messages.dto.pagination_model import pagination_model
# from chat_api.api.messages.dto.message_owner_model import message_owner_model
# from chat_api.api.messages.dto.message_model import message_model
# from chat_api.api.messages.dto.pagination_link_model import pagination_links_model

segmentation_ns = Namespace(name="segmentation", validate=True)
# timeseries_ns.models[message_owner_model.name] = message_owner_model
# timeseries_ns.models[message_model.name] = message_model
# timeseries_ns.models[pagination_links_model.name] = pagination_links_model
# timeseries_ns.models[pagination_model.name] = pagination_model

import numbers_segmentation_api.api.segmentation.endpoints.add_task
import numbers_segmentation_api.api.segmentation.endpoints.get_status

