from http import HTTPStatus
import json
from flask_restx import Resource
from flask import jsonify, request
import time
from werkzeug.utils import secure_filename
import os
import uuid
from PIL import Image
from flask_restx import marshal

from numbers_segmentation_api.api.segmentation.dto.add_task_request import upload_parser
from numbers_segmentation_api.api.segmentation.dto.add_task_response import add_task_response

# from numbers_segmentation_api.api.timeseries.business.create_timeseries import create_timeseries

from numbers_segmentation_api import redis_queue, app
from numbers_segmentation_api.queue.tasks.segmentation import segmentation
from numbers_segmentation_api.api.segmentation.endpoints import segmentation_ns

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'bmp'])
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@segmentation_ns.route("/task/create", endpoint="prediction_task_create")
@segmentation_ns.response(int(HTTPStatus.OK), "Added tasks.", add_task_response)
@segmentation_ns.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
@segmentation_ns.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
class TimeseriesCreate(Resource):
    """Handles HTTP requests to URL: /task/create."""

    # @segmentation_ns.doc(security="Bearer")
    # @segmentation_ns.response(int(HTTPStatus.FORBIDDEN), "Token is required.")
    # @segmentation_ns.response(int(HTTPStatus.OK), "Added new timeseries.")
    @segmentation_ns.expect(upload_parser, validate=True)
    @segmentation_ns.doc('segmentation_task_create', description='Создать задачу для сегментации')
    # @segmentation_ns.expect(create_timeseries_req, validate=True)
    def post(self):
        """Создать задачу для сегментации"""
        files = upload_parser.parse_args()['files']
        if len(files) > 10:
            response = jsonify(message="allowed upload max 10 files")
            response.status_code = HTTPStatus.BAD_REQUEST
            return response

        for file in files:
            if not allowed_file(file.filename):
                allowed = ",".join(ALLOWED_EXTENSIONS)
                response = jsonify(message=f"allowed extantions is {allowed}")
                response.status_code = HTTPStatus.BAD_REQUEST
                return response

        result = []
        for file in files:
            filename = str(uuid.uuid4())+file.filename
            filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(filepath)

            im = Image.open(filepath)
            width, height = im.size
            file_result = {"job_id":"", "image_path":"", "error_message":""}
            if width != height:
                file_result["image_path"] = "/uploads/" + filename
                file_result["error_message"] = "prediction work only on square image"
                result.append(file_result)
                continue

            try:
                job = redis_queue.enqueue_call(
                    func=segmentation, args=(filepath,), result_ttl=5000
                )
                file_result["image_path"] = "/uploads/" + filename
                file_result["job_id"] = job.get_id()
                result.append(file_result)
            except Exception as e:
                file_result["image_path"] = "/uploads/" + filename
                file_result["error_message"] = f"error when try to create task: {str(e)}"
                result.append(file_result)

        response = {"result": result}
        response_data = marshal(response, add_task_response)
        return jsonify(response_data)