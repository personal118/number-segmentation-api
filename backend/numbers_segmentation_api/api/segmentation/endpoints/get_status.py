from http import HTTPStatus
import json
from flask_restx import Resource
from flask import jsonify, request
import time
from werkzeug.utils import secure_filename
import os
import uuid
from PIL import Image
from flask_restx import marshal

from numbers_segmentation_api.api.segmentation.dto.get_status_request import get_status_request, get_status_reqparser
from numbers_segmentation_api.api.segmentation.dto.get_status_response import get_status_response

# from numbers_segmentation_api.api.timeseries.business.create_timeseries import create_timeseries

from numbers_segmentation_api import redis_queue, app
from numbers_segmentation_api.queue.tasks.segmentation import segmentation
from numbers_segmentation_api.api.segmentation.endpoints import segmentation_ns


@segmentation_ns.route("/task/result", endpoint="prediction_task_status")
@segmentation_ns.response(int(HTTPStatus.OK), "Returned status.", get_status_response)
@segmentation_ns.response(int(HTTPStatus.BAD_REQUEST), "Validation error.")
@segmentation_ns.response(int(HTTPStatus.INTERNAL_SERVER_ERROR), "Internal server error.")
class TaskResult(Resource):
    """Handles HTTP requests to URL: /task/result."""

    # @segmentation_ns.doc(security="Bearer")
    # @segmentation_ns.response(int(HTTPStatus.FORBIDDEN), "Token is required.")
    # @segmentation_ns.response(int(HTTPStatus.OK), "Added new timeseries.")
    @segmentation_ns.expect(get_status_request, validate=True)
    @segmentation_ns.doc('segmentation_task_status', description='Получить результат задачи для сегментации')
    @segmentation_ns.expect(get_status_reqparser, validate=True)
    def post(self):
        """Получить статус задачи для сегментации"""
        req_dict = get_status_reqparser.parse_args()
        job_ids = req_dict["job_ids"]
        result = []
        for job_id in job_ids:
            job_id = str(job_id)
            job_result = {
                'job_id': "",
                'job_status': "",
                'job_result': "",
                'error_message': "",
            }
            try:
                job = redis_queue.fetch_job(job_id)
                job_result["job_id"] = job_id
                job_result["job_status"] = job.get_status()
                job_result["job_result"] = job.result
                result.append(job_result)
            except Exception as e:
                job_result["job_id"] = job_id
                job_result["error_message"] = str(e)
                result.append(job_result)

        response = {"result": result} 
        response_data = marshal(response, get_status_response)
        return jsonify(response_data)
