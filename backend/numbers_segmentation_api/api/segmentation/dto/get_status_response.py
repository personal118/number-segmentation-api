from flask_restx import fields

from numbers_segmentation_api.api.segmentation.endpoints import segmentation_ns

task_status = segmentation_ns.model('JobStatus', {
    'job_id': fields.String,
    'job_status': fields.String,
    'job_result': fields.String,
    'error_message': fields.String,
})

get_status_response = segmentation_ns.model('GetStatusResponse', {
    'result': fields.List(fields.Nested(task_status)),
})
