from flask_restx import fields

from numbers_segmentation_api.api.segmentation.endpoints import segmentation_ns

job_add = segmentation_ns.model('JobAdd', {
    'job_id': fields.String,
    'image_path': fields.String,
    'error_message': fields.String,
})

add_task_response = segmentation_ns.model('AddTaskResponse', {
    'result': fields.List(fields.Nested(job_add)),
})


