from typing import List
from flask_restx import fields
from flask_restx.reqparse import RequestParser

from numbers_segmentation_api.api.segmentation.endpoints import segmentation_ns
from werkzeug.datastructures import FileStorage

upload_parser = RequestParser(bundle_errors=True)
upload_parser.add_argument('files', type=FileStorage, location='files',required=True,action='append')
# upload_parser.add_argument('file', location='files',
#                            type=FileStorage, required=True)