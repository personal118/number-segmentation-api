from typing import List
from flask_restx import fields
from flask_restx.reqparse import RequestParser

from numbers_segmentation_api.api.segmentation.endpoints import segmentation_ns

get_status_request = segmentation_ns.model('GetTaskStatusRequest', {
    'job_ids': fields.List(fields.String),
})

def job_ids(t):
	return t

get_status_reqparser = RequestParser(bundle_errors=True)
get_status_reqparser.add_argument(
    "job_ids",
    type=job_ids,
    location="json",
    required=True,
    nullable=False,
)
