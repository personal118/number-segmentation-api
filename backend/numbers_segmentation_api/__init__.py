"""Flask app initialization via factory pattern."""
from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
# from flask_oidc import OpenIDConnect
import logging
import os
from prometheus_flask_exporter.multiprocess import GunicornInternalPrometheusMetrics
from flask_healthz import healthz, HealthError
from numbers_segmentation_api import config
import redis
from rq import Queue
from rq.job import Job

app = Flask("numbers-segmentation-api",  static_url_path='', static_folder='static')
cors = CORS()
db = SQLAlchemy()
migrate = Migrate()
metrics = GunicornInternalPrometheusMetrics.for_app_factory()
redis_queue = Queue(connection=redis.from_url(config.get_config(os.getenv('FLASK_ENV', 'development')).REDIS_URI))
# oidc = OpenIDConnect()

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s]: {} %(levelname)s %(message)s'.format(os.getpid()),
                    datefmt='%Y-%m-%d %H:%M:%S',
                    handlers=[logging.StreamHandler()])
logger = logging.getLogger()

def printok():
    print("Everything is fine")

def liveness():
    try:
        db.engine.execute('SELECT 1')
    except Exception as e:
        raise HealthError(f"Can't connect to database {app.config}")

def readiness():
    printok()

def create_app(config_name):
    logger.info(f'Starting app in {config_name} environment')

    app.config.from_object(config.get_config(config_name))
    app.config.update(
        HEALTHZ = {
            "live": "numbers_segmentation_api.liveness",
            "ready": "numbers_segmentation_api.readiness",
        }
    )

    # logger.info(f'DB {config.get_config(config_name).SQLALCHEMY_DATABASE_URI} environment')

    from numbers_segmentation_api.api import api_bp
    app.register_blueprint(api_bp)
    app.register_blueprint(healthz, url_prefix="/healthz")

    cors.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    metrics.init_app(app)
    # oidc.init_app(app)

    return app
