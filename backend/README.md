# Backend

Реализует JWT авторизацию и простой доступ к ресурсам (сообщения). Для сообщений реализован не полный набор CRUD, т.е. только Create и Read.

## Переменные окружения

`SECRET_KEY={секретный ключ участвующий в формировании JWT токена}`    
`DATABASE_URL={креды для подключения к БД, например (postgresql://postgres:postgres@localhost:5432/)}`  

## Migrations

Инициализация БД

`python -m flask db init`    
`python -m flask db upgrade`  

Для отката / наката миграций  
`python -m flask db downgrade`    
`python -m flask db upgrade`  