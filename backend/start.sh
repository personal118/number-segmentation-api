#!/bin/bash

python ./numbers_segmentation_api/queue/worker.py &
gunicorn -c ./numbers_segmentation_api/wsgi/config.py --worker-class gevent --workers 2 --bind 0.0.0.0:5000 numbers_segmentation_api.wsgi:app --max-requests 10000 --timeout 5 --keep-alive 5 --log-level info &

# Wait for any process to exit
wait
  
# Exit with status of process that exited first
exit $?
